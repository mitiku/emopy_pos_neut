EMOTIONS = {
    # 0 : 'anger', 
    # 1 : 'disgust', 
    # 2 : 'fear', 
    3 : 'happy', 
    # 4 : 'sad', 
    5 : 'surprise', 
    6 : 'neutral'
}
EMOTION_STATES ={
    # "anger"     :1,
    # "disgust"   :1,
    # "fear"      :1,
    "happy"     :1,
    # "sad"       :1,
    "surprise"  :1,
    "neutral"   :0
}
IMG_SIZE = (48, 48)

THRESHOLDS = {
    0 : 0.5, 
    1 : 0.5, 
    2 : 0.5, 
    3 : 0.5, 
    4 : 0.5, 
    5 : 0.5, 
    6 : 0.5
}