from constants import EMOTIONS,IMG_SIZE,EMOTION_STATES
import cv2
import os
import numpy as np
import dlib

def sanitize(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # Convert to grayscale
    img = cv2.resize(img, IMG_SIZE)  # Resize
    return img


def emotion_to_string(emotion):
    return EMOTIONS[emotion]
def get_emotion_state(emotion):
    return EMOTION_STATES[emotion]

def string_to_emotion(string):
    for emotion in EMOTIONS:
        if EMOTIONS[emotion] == string:
            return emotion
    raise Exception("value "+string," does not exist")
def load_dataset(directory,verbose=True):
    x, y = [], []

    # Read images from the directory
    for emotion_dir in os.listdir(directory):
        if not emotion_dir in EMOTIONS.values():
            continue
        if verbose:
            print "loading",emotion_dir,"dataset"
        for filename in os.listdir(os.path.join(directory, emotion_dir)):
            try:
                x += [sanitize(cv2.imread(os.path.join(directory, emotion_dir, filename)))]
            except cv2.error,e:
                print "Error while reading ", os.path.join(directory, emotion_dir, filename)
                continue
            # y += [string_to_emotion(emotion_dir)]
            y +=[get_emotion_state(emotion_dir)]
            

    # Convert to numpy array
    x = np.array(x, dtype='uint8')
    y = np.array(y)

    return x, y
def load_lbp_dataset(directory,verbose=True,number_of_points=8):
    x, y = [], []

    # Read images from the directory
    for emotion_dir in os.listdir(directory):

        
        if verbose:
            print "loading",emotion_dir,"dataset"
        for filename in os.listdir(os.path.join(directory, emotion_dir)):
            try:
                image =sanitize(cv2.imread(os.path.join(directory, emotion_dir, filename)))
                x += [to_multi_dimensional_lbp(image,num_points=number_of_points)]
            except cv2.error,e:
                print "Error while reading ", os.path.join(directory, emotion_dir, filename)
                continue
            y += [string_to_emotion(emotion_dir)]
            

    # Convert to numpy array
    x = np.array(x, dtype='uint8')
    y = np.array(y)

    return x, y
def binarize(x, y, emotion,verbose=True):
    if verbose:
        print "binarizing dataset"
    positives = (y == emotion)
    xp = x[positives]

    xn = x[np.invert(positives)]
    xn = xn[np.random.choice(xn.shape[0], xp.shape[0], replace=False)]

    x = np.concatenate((xp, xn), axis=0)
    y = np.concatenate((np.ones(xp.shape[0], dtype=np.int), np.zeros(xn.shape[0], dtype=np.int)), axis=0)
    y = np.eye(2)[y]

    return x, y
def get_dlib_points(image,predictor):
    face = dlib.rectangle(0,0,image.shape[1]-1,image.shape[0]-1)
    img = image.reshape(IMG_SIZE[0],IMG_SIZE[1])
    shapes = predictor(img,face)
    parts = shapes.parts()
    output = np.zeros((68,2))
    for i,point in enumerate(parts):
        output[i]=[point.x,point.y]
    output = np.array(output).reshape((1,68,2))
    return output
def to_dlib_points(images,predictor):
    output = np.zeros((len(images),1,68,2))
    centroids = np.zeros((len(images),2))
    for i in range(len(images)):
        dlib_points = get_dlib_points(images[i],predictor)[0]
        centroid = np.mean(dlib_points,axis=0)
        centroids[i] = centroid
        output[i][0] = dlib_points
    return output,centroids
        
def get_distances_angles(all_dlib_points,centroids):
    all_distances = np.zeros((len(all_dlib_points),1,68,1))
    all_angles = np.zeros((len(all_dlib_points),1,68,1))
    for i in range(len(all_dlib_points)):
        dists = np.linalg.norm(centroids[i]-all_dlib_points[i][0],axis=1)
        angles = get_angles(all_dlib_points[i][0],centroids[i])
        all_distances[i][0] = dists.reshape(1,68,1)
        all_angles[i][0] = angles.reshape(1,68,1)
    return all_distances,all_angles
def angle_between(p1, p2):
    ang1 = np.arctan2(*p1[::-1])
    ang2 = np.arctan2(*p2[::-1])
    return (ang1 - ang2) % (2 * np.pi)
def get_angles(dlib_points,centroid):
    output = np.zeros((68))
    for i in range(68):
        angle = angle_between(dlib_points[i],centroid)
        output[i] = angle
    return output