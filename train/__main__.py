from trainers import Trainer,TrainerWithDropout,TrainerWithAugmentationAndDroupout,AllVsAllTrainer,LbpTrainer,LbpAllVsAllTrainer,SimpleModelAVATrainer,MultipleRepresentationTrainer


# trainer = Trainer()
# trainer = TrainerWithAugmentationAndDroupout()
# trainer = TrainerWithAugmentation()
# trainer = LbpTrainer(dataset = "ck",batch_size=32,epochs=80,learnining_rate = 1e-4,log_file = "mdlbp-dr")
# trainer.train()

dataset = "all"
learnining_rate = 1e-4
batch_size = 32
epochs = 250
tr = 7
steps_per_epoch = 1000
# models_prefix = "fer-baseline"
# salcsin123
if tr ==0:
    trainer = Trainer(dataset=dataset,
                learnining_rate=learnining_rate,
                batch_size=batch_size,
                epochs=epochs)
    
elif tr==1:
    trainer = TrainerWithDropout(dataset=dataset,
                learnining_rate=learnining_rate,
                batch_size=batch_size,
                epochs=epochs)
elif tr==2:
    trainer = TrainerWithAugmentationAndDroupout(dataset=dataset,
                learnining_rate=learnining_rate,
                batch_size=batch_size,
                epochs=epochs,steps_per_epoch = steps_per_epoch)
elif tr==3:
    trainer = LbpTrainer(dataset=dataset,
                learnining_rate=learnining_rate,
                batch_size=batch_size,
                epochs=epochs)
elif tr==4:
    trainer = LbpAllVsAllTrainer(dataset=dataset,
                models_prefix = "mc",
                models_suffix = "lbp",
                learnining_rate=learnining_rate,
                batch_size=batch_size,
                epochs=epochs)
elif tr==5:
    trainer = AllVsAllTrainer(dataset=dataset,
                learnining_rate=learnining_rate,
                batch_size=batch_size,
                epochs=epochs,steps_per_epoch = steps_per_epoch,
                models_prefix = "all",
                models_suffix = "ag_dr",
                model_name_suffix = "all_ag",
                augmentation=True,
                number_of_layers_to_fine_tune = 3
                )
elif tr == 6:
    trainer = SimpleModelAVATrainer(dataset=dataset,
                learnining_rate=learnining_rate,
                batch_size=batch_size,
                epochs=epochs,
                model_name = "simple-ava-ag",
                # augmentation = True,
                steps_per_epoch= steps_per_epoch
                )
elif tr == 7:
    trainer = MultipleRepresentationTrainer(dataset=dataset,
                learnining_rate=learnining_rate,
                batch_size=batch_size,
                epochs=epochs,
                model_name = "neut-pos",
                augmentation = True,
                steps_per_epoch = steps_per_epoch
                )
trainer.train()