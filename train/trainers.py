import cv2,dlib
import numpy as np
import os
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

import keras
from keras.models import Sequential, model_from_json, Model
from keras.layers import Dense, Dropout, Flatten, Input
from keras.layers import Conv2D, MaxPooling2D
from util.util import load_dataset,sanitize,binarize,emotion_to_string, load_lbp_dataset
from util.util import get_angles, get_distances_angles,to_dlib_points
from util.constants import IMG_SIZE
import datetime
from keras.preprocessing.image import ImageDataGenerator
from util.constants import EMOTIONS
from scipy.ndimage import interpolation
from keras import  callbacks

BATCH_SIZE = 64
EPOCHS = 100
class Trainer(object):
    def __init__(self,
            epochs=EPOCHS,batch_size=BATCH_SIZE,
            model_path="models",
            dataset_dir="dataset",
            dataset="ck",
            model_name_suffix = "se",
            verbose = True,
            learnining_rate = 1e-4
            ):
        self.epochs = epochs
        self.batch_size = batch_size
        self.model_path = model_path
        self.dataset_dir = dataset_dir
        self.dataset = dataset
        self.model_name_suffix = model_name_suffix
        self.verbose = verbose
        self.learnining_rate = learnining_rate
    def get_model(self):
        model = Sequential()
        model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(IMG_SIZE[0], IMG_SIZE[1], 1)))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.2))
        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        model.add(Dropout(0.2))
        model.add(Dense(2, activation='softmax'))

        model.compile(loss=keras.losses.categorical_crossentropy,
                    optimizer=keras.optimizers.Adadelta(),
                    metrics=['accuracy'])
        return model
    def get_datasets(self):
        print self.dataset_dir,self.dataset,"train"
        x_train_all , y_train_all = load_dataset(os.path.join(self.dataset_dir,self.dataset,"train"))
        x_test_all , y_test_all = load_dataset(os.path.join(self.dataset_dir,self.dataset,"test"))

        x_train_all, y_train_all = shuffle(x_train_all,y_train_all)
        x_test_all , y_test_all  = shuffle(x_test_all, y_test_all)
        x_train_all = x_train_all.reshape((-1,IMG_SIZE[0],IMG_SIZE[1],1)).astype("float32")/255
        x_test_all  = x_test_all.reshape((-1,IMG_SIZE[0],IMG_SIZE[1],1)).astype("float32")/255
        
        return x_train_all,x_test_all,y_train_all,y_test_all
    def save_and_log_model(self,model,model_name,x_test,y_test):
        model_json = model.to_json()
        with open( os.path.join(self.model_path ,self.dataset+"-" + model_name +"-" +self.model_name_suffix+ ".json"), "w+") as json_file:
            json_file.write(model_json)
        model.save_weights(os.path.join(self.model_path ,self.dataset +"-" + model_name +"-" +self.model_name_suffix+ ".h5"))
        score = model.evaluate(x_test, y_test, batch_size=self.batch_size)
        with open(os.path.join("logs",self.dataset +"-" + self.model_name_suffix+".txt"),"a+") as logfile:
            logfile.write(model_name+":")
            logfile.write(str(score))
            logfile.write("\n")
        if self.verbose:
            print("Saved model to disk")

    def train(self):
        
        x_train_all,x_test_all,y_train_all,y_test_all = self.get_datasets()
        with open(os.path.join("logs",self.dataset +"-" + self.model_name_suffix+".txt"),"a+") as logfile:
            if self.verbose:
                print "logs are written to ",os.path.join("logs",self.dataset +"-" + self.model_name_suffix+".txt")
            a = datetime.datetime.now()
            a = a.strftime("%a %b %d %Y %H:%M:%S")
            logfile.write("_"*(2*len(a))+"\n")
            logfile.write(a)
            logfile.write("\n")
            logfile.write("Training on dataset: "+self.dataset + ", With the following parameters")
            logfile.write("\n")
            logfile.write("-"*len(a))
            logfile.write("\n")
            logfile.write("Epochs "+str(self.epochs))
            logfile.write("\n")
            logfile.write("Batch-size "+str(self.batch_size))
            logfile.write("\n")
            try:
                logfile.write("Learning-rate "+self.learnining_rate)
            except:
                pass
            
            logfile.write("-"*len(a))
            logfile.write("\n")
            
        for i in range(7):
            if self.verbose:
                print "Modeling ", emotion_to_string(i)

            x_train, y_train = binarize(x_train_all, y_train_all, i)
            x_test, y_test = binarize(x_test_all, y_test_all,i)
        
            model = self.get_model()
            model.fit(x_train, y_train,
                    batch_size=self.batch_size,
                    epochs=self.epochs,
                    verbose=1,
                    validation_data=(x_test, y_test))
            self.save_and_log_model(model,emotion_to_string(i),x_test,y_test)
            # Save model to file

class EmopyMultiRepDataGenerator(ImageDataGenerator):
    def __init__(self,
                 featurewise_center=False,
                 samplewise_center=False,
                 featurewise_std_normalization=False,
                 samplewise_std_normalization=False,
                 zca_whitening=False,
                 zca_epsilon=1e-6,
                 rotation_range=0.,
                 width_shift_range=0.,
                 height_shift_range=0.,
                 shear_range=0.,
                 zoom_range=0.,
                 channel_shift_range=0.,
                 fill_mode='nearest',
                 cval=0.,
                 horizontal_flip=False,
                 vertical_flip=False,
                 rescale=None,
                 preprocessing_function=None,
                 data_format=None,
                 batch_size = BATCH_SIZE,
                 shuffle = True,
                 predictor= None
                 ):
        ImageDataGenerator.__init__(self,featurewise_center,
                 samplewise_center,
                 featurewise_std_normalization,
                 samplewise_std_normalization,
                 zca_whitening,
                 zca_epsilon,
                 rotation_range,
                 width_shift_range,
                 height_shift_range,
                 shear_range,
                 zoom_range,
                 channel_shift_range,
                 fill_mode,
                 cval,
                 horizontal_flip,
                 vertical_flip,
                 rescale,
                 preprocessing_function,
                 data_format)
        self.predictor = predictor
        self.batch_size = batch_size
        self.shuffle = shuffle

    def get_exploration_order(self,x):
        indexes = np.arange(len(x))
        if self.shuffle:
            np.random.shuffle(indexes)
        return indexes

    def generate(self,x,y):
        while True:
            indexes = self.get_exploration_order(x)
            iMax = int(len(indexes)/self.batch_size)
            for i in range(iMax):
                temp_indexes = indexes[i*self.batch_size:(i+1)*self.batch_size]
                x_temp = x[temp_indexes]
                y_temp = y[temp_indexes]
                X, DlibPoints,Dists,Angles,Y = self.datageneration(x_temp,y_temp)
                X = X.astype(float)/255
                DlibPoints /= 50.0;
                DlibPoints = DlibPoints.reshape((-1,1,68,2))
                Dists /= 50.0;
                Dists = Dists.reshape(-1,1,68,1)
                Angles /= (2 * np.pi)
                Angles = Angles.reshape(-1,1,68,1)
                X = X.reshape(-1,48,48,1)
                yield  [X, DlibPoints,Dists,Angles],Y
    def datageneration(self,x,y):
        X = np.zeros((x.shape[0],48,48))
        Y = y
        for i in range(len(x)):
            current_x = self.random_transform(x[i])
            current_x = current_x.reshape((48,48))
            X[i] = current_x
        X = X.astype(np.uint8)

        DlibPoints,Centroids = to_dlib_points(X,self.predictor)
        Dists,Angles = get_distances_angles(DlibPoints,Centroids)
            
        return X,DlibPoints,Dists,Angles,Y

class TrainerWithDropout(Trainer):
    def __init__(self,
            epochs=EPOCHS,batch_size=BATCH_SIZE,
            model_path="models",dataset_dir="dataset",
            dataset = "ck",
            model_name_suffix = "dr",
            verbose = True,
            learnining_rate = 1e-4
            ):   
        Trainer.__init__(self,epochs,batch_size,
            model_path,dataset_dir,
            dataset,
            model_name_suffix,
            verbose,
            learnining_rate
             )
    def get_model(self):
        model = Sequential()
        model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(IMG_SIZE[0], IMG_SIZE[1], 1)))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(Dropout(0.2))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(128, (3, 3), activation='relu'))
        model.add(Dropout(0.2))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(128, (3, 3), activation='relu'))
        model.add(Dropout(0.2))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        model.add(Dense(252, activation='relu'))
        model.add(Dropout(0.2))
        model.add(Dense(2, activation='softmax'))
        model.compile(loss=keras.losses.categorical_crossentropy,
                    optimizer=keras.optimizers.Adam(self.learnining_rate),
                    metrics=['accuracy'])
        return model

class TrainerWithAugmentation(Trainer):
    def __init__(self,
            epochs=10,batch_size=32,
            model_path="models",dataset_dir="dataset",
            dataset = "ck",
            model_name_suffix = "ag",
            verbose = True,
            steps_per_epoch = 400,
            learnining_rate = 1e-4
            
            ):   
        Trainer.__init__(self,epochs,batch_size,
            model_path,dataset_dir,
            dataset,
            model_name_suffix,
            verbose,
            learnining_rate
            )
        self.steps_per_epoch = steps_per_epoch
    def train(self):
        
        x_train_all,x_test_all,y_train_all,y_test_all = self.get_datasets()
        if self.verbose:
            print "training on",x_train_all.shape[0]
            print "testing on ",x_test_all.shape[0]
        with open(os.path.join("logs",self.dataset +"-" + self.model_name_suffix+".txt"),"a+") as logfile:
            if self.verbose:
                print "logs are written to ",os.path.join("logs",self.dataset +"-" + self.model_name_suffix+".txt")
            a = datetime.datetime.now()
            a = a.strftime("%a %b %d %Y %H:%M:%S")
            logfile.write("_"*(2*len(a))+"\n")
            logfile.write(a)
            logfile.write("\n")
            logfile.write("Training on dataset: "+self.dataset + ", With the following parameters")
            logfile.write("\n")
            logfile.write("-"*len(a))
            logfile.write("\n")
            logfile.write("Epochs "+str(self.epochs))
            logfile.write("\n")
            logfile.write("Batch-size "+str(self.batch_size))
            logfile.write("\n")
            try:
                logfile.write("Learning-rate "+self.learnining_rate)
            except:
                pass
            
            logfile.write("-"*len(a))
            logfile.write("\n")
            
        for i in range(7):
            if i<4:
                continue
            if self.verbose:
                print "Modeling ", emotion_to_string(i)
            x_train, y_train = binarize(x_train_all, y_train_all, i)
            x_test, y_test = binarize(x_test_all, y_test_all,i)
            print "Using Augmentation"
            datagen=ImageDataGenerator(
                rotation_range=20,
                horizontal_flip=True,
                shear_range=0.2,
                width_shift_range=0.2,
                height_shift_range=0.2,
                zoom_range=0.2,
                # equalize_adapthist=True
            )
            datagen.fit(x_train)
            model = self.get_model()
            model.fit_generator(datagen.flow(x_train,y_train,
                batch_size=self.batch_size
            ),steps_per_epoch=self.steps_per_epoch,
                        epochs=self.epochs,
                        validation_data=(x_test, y_test))
            self.save_and_log_model(model,emotion_to_string(i),x_test,y_test)
class TrainerWithAugmentationAndDroupout(TrainerWithAugmentation,TrainerWithDropout):
    def __init__(self,
            epochs=10,batch_size=BATCH_SIZE,
            model_path="models",dataset_dir="dataset",
            dataset="ck",
            model_name_suffix = "ag_dr",
            verbose = True,
            steps_per_epoch = 1000,
            learnining_rate = 1e-5
            ):
        TrainerWithDropout.__init__(self,epochs,batch_size,
            model_path,dataset_dir,
            dataset,
            model_name_suffix,
            verbose,
            learnining_rate
             )
        TrainerWithAugmentation.__init__(self,epochs,batch_size,
            model_path,dataset_dir,
            dataset,
            model_name_suffix,
            verbose,
            steps_per_epoch,
            learnining_rate
             )
class AllVsAllTrainer(Trainer):
    def __init__(self,
                epochs = 40,
                batch_size = BATCH_SIZE,
                models_path = "models",
                model_path = "models",
                dataset_dir = "dataset",
                dataset = "ck",
                model_name_suffix = "basic",
                verbose = True,
                learnining_rate = 1e-4,
                models_suffix = "se",
                models_prefix = "ck",
                augmentation = False,
                steps_per_epoch = 200,
                number_of_layers_to_fine_tune = 0
                ):

        Trainer.__init__(self,epochs,batch_size,
            model_path,dataset_dir,
            dataset,
            model_name_suffix,
            verbose,
            learnining_rate
        )
        self.input_shape = (IMG_SIZE[0],IMG_SIZE[1],1)
        self.models_path = models_path
        self.models_suffix = models_suffix
        self.models_prefix = models_prefix
        self.augmentation = augmentation
        self.steps_per_epoch = steps_per_epoch
        self.number_of_layers_to_fine_tune = number_of_layers_to_fine_tune
    def get_model(self,fine_tune=False):
        print "number of layers to fine tune",self.number_of_layers_to_fine_tune
        print "self.input_shape",self.input_shape
        models = []
        models_input = Input(shape= self.input_shape)
        for emotion in EMOTIONS:
            with open(os.path.join(self.models_path,self.models_prefix+"-" + emotion_to_string(emotion) +"-"+self.models_suffix+".json")) as modelfile:
                model = model_from_json(modelfile.read())
                model.load_weights(os.path.join(self.models_path,self.models_prefix+"-" + emotion_to_string(emotion) +"-"+self.models_suffix+".h5"))
                models.append(model)
        layers = []
        for model in models:
            if fine_tune:
                for i in range(len(model.layers)):
                    if i<len(model.layers)-self.number_of_layers_to_fine_tune:
                        model.layers[i].trainable=False
                    else:
                        model.layers[i].trainable= True
            else:
                for i in range(len(model.layers)):
                    model.layers[i].trainable=False
            model_to_layer = model(models_input)
            layers.append(model_to_layer)
        merged_layers = keras.layers.concatenate(layers,axis=-1)
        fc1 = Dense(64,activation="relu")(merged_layers)
        fcd1 = Dropout(0.2)(fc1)
        fc2 = Dense(64,activation="relu")(fcd1)
        fcd2 = Dropout(0.2)(fc2)
        fc3 = Dense(128,activation="relu")(fcd2)
        fcd3 = Dropout(0.2)(fc3)
        fc4 = Dense(252,activation="relu")(fcd3)
        fcd4 = Dropout(0.2)(fc4)
        fc5 = Dense(252,activation="relu")(fcd4)
        fcd5 = Dropout(0.2)(fc5)
        fc6 = Dense(1024,activation="relu")(fcd5)
        fcd6 = Dropout(0.2)(fc6)
        predictions = Dense(7,activation="softmax")(fcd6)
        model = Model(inputs=models_input,outputs=predictions)
        model.compile(loss=keras.losses.categorical_crossentropy,
                    optimizer=keras.optimizers.Adam(self.learnining_rate),
                    metrics=['accuracy'])
        return model
    
    
    def save_and_log_model(self,model,model_name,x_test,y_test):
        print "save of ava"
        model_json = model.to_json()
        with open( os.path.join(self.model_path ,self.dataset+"-" + model_name +"-" +self.model_name_suffix+ ".json"), "w+") as json_file:
            json_file.write(model_json)
        model.save_weights(os.path.join(self.model_path ,self.dataset+"-"+ model_name +"-" +self.model_name_suffix+ ".h5"))
        score = model.evaluate(x_test, y_test, batch_size=self.batch_size)
        
        with open(os.path.join("logs",self.dataset + "-" + self.model_name_suffix+".txt"),"a+") as logfile:
            logfile.write("dataset - "+self.dataset+" : ")
            a = datetime.datetime.now()
            logfile.write(a.strftime("%a %b %d %Y %H:%M:%S"))
            logfile.write(":")
            logfile.write(model_name+":")
            logfile.write(str(score))
            logfile.write("\n")
        if self.verbose:
            print("Saved model to disk")

    def train(self):
        print "train of ava"
        if self.number_of_layers_to_fine_tune>0:
            fine_tune = True
        else:
            fine_tune = False
        model = self.get_model(fine_tune=fine_tune)
        x_train,x_test,y_train,y_test = self.get_datasets()
        y_train = np.eye(7)[y_train]
        y_test = np.eye(7)[y_test]
        with open(os.path.join("logs",self.dataset + "-" + self.model_name_suffix+".txt"),"a+") as logfile:
            if self.verbose:
                print "logs are written to ",os.path.join("logs",self.dataset + "-" + self.model_name_suffix+".txt")
            a = datetime.datetime.now()
            a = a.strftime("%a %b %d %Y %H:%M:%S")
            logfile.write("_"*(2*len(a))+"\n")
            logfile.write(a)
            logfile.write("\n")
            logfile.write("Training all vs all model on dataset: "+self.dataset + ", With the following parameters")
            logfile.write("\n")
            logfile.write("-"*len(a))
            logfile.write("\n")
            logfile.write("Epochs "+str(self.epochs))
            logfile.write("\n")
            logfile.write("Batch-size "+str(self.batch_size))
            logfile.write("\n")
            try:
                logfile.write("Learning-rate "+ str(self.learnining_rate))
                logfile.write("\n")
            except Exception ,e :
                print e.message
                pass
            
            logfile.write("-"*len(a))
            logfile.write("\n")
        if not self.augmentation:
            model.fit(x_train, y_train,
                batch_size=self.batch_size,
                epochs=self.epochs,
                verbose=1,
                validation_data=(x_test, y_test))
        else:
            print "Using image augmentation"
            datagen=ImageDataGenerator(
                rotation_range=20,
                horizontal_flip=True,
                shear_range=0.2,
                width_shift_range=0.2,
                height_shift_range=0.2,
                zoom_range=0.2,
                featurewise_center=True,
                featurewise_std_normalization=True,
                # equalize_adapthist=True

            )
            datagen.fit(x_train)
            model.fit_generator(datagen.flow(x_train,y_train,
                batch_size=self.batch_size
            ),steps_per_epoch=self.steps_per_epoch,
                        epochs=self.epochs,
                        validation_data=(x_test, y_test))
        self.save_and_log_model(model,self.model_name_suffix,x_test,y_test)
class LbpTrainer(TrainerWithDropout):
    def __init__(self,
                epochs = 40,
                batch_size = BATCH_SIZE,
                models_path = "models",
                model_path = "models",
                dataset_dir = "dataset",
                dataset = "ck",
                model_name_suffix = "lbp",
                verbose = True,
                learnining_rate = 1e-4,
                number_of_points = 8
                ):
        
        TrainerWithDropout.__init__(self,epochs,batch_size,
            model_path,dataset_dir,
            dataset,
            model_name_suffix,
            verbose,
            learnining_rate,
            
        )
        self.number_of_points = number_of_points
        self.input_shape = (IMG_SIZE[0],IMG_SIZE[1],self.number_of_points)
    def get_datasets(self):
        print "get dataset of lbp trainer"
        x_train_all , y_train_all = load_lbp_dataset(os.path.join(self.dataset_dir,self.dataset,"train"))
        x_test_all , y_test_all = load_lbp_dataset(os.path.join(self.dataset_dir,self.dataset,"test"))

        x_train_all, y_train_all = shuffle(x_train_all,y_train_all)
        x_test_all , y_test_all  = shuffle(x_test_all, y_test_all)
        x_train_all = x_train_all.reshape((-1,self.input_shape[0],self.input_shape[1],self.input_shape[2])).astype("float32")/255
        x_test_all  = x_test_all.reshape((-1,self.input_shape[0],self.input_shape[1],self.input_shape[2])).astype("float32")/255
        
        return x_train_all,x_test_all,y_train_all,y_test_all
    def get_model(self,num_points = 8):
        print "get model of lbp trainer"
        model = Sequential()
        model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=self.input_shape))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(Dropout(0.2))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(128, (3, 3), activation='relu'))
        model.add(Dropout(0.2))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(128, (3, 3), activation='relu'))
        model.add(Dropout(0.2))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        model.add(Dense(252, activation='relu'))
        model.add(Dropout(0.2))
        model.add(Dense(2, activation='softmax'))
        model.compile(loss=keras.losses.categorical_crossentropy,
                    optimizer=keras.optimizers.Adam(self.learnining_rate),
                    metrics=['accuracy'])
        return model

class LbpAllVsAllTrainer(LbpTrainer,AllVsAllTrainer):
    def __init__(self,
                epochs = 40,
                batch_size = BATCH_SIZE,
                models_path = "models",
                model_path = "models",
                dataset_dir = "dataset",
                dataset = "ck",
                model_name_suffix = "lbp-ava",
                verbose = True,
                learnining_rate = 1e-4,
                models_suffix = "lbp",
                models_prefix = "ck",
                augmentation = False,
                steps_per_epoch = 200
                ):
        LbpTrainer.__init__(self,
                    epochs,batch_size,
                    models_path,
                    model_path,
                    dataset_dir,
                    dataset,
                    model_name_suffix,
                    verbose,
                    learnining_rate,
                    )
        AllVsAllTrainer.__init__(self,
                    epochs,batch_size,
                    models_path,
                    model_path,
                    dataset_dir,
                    dataset,
                    model_name_suffix,
                    verbose,
                    learnining_rate,
                    models_suffix = models_suffix,
                    models_prefix = models_prefix,
                    augmentation = augmentation,
                    steps_per_epoch = steps_per_epoch
        )
        self.input_shape = (IMG_SIZE[0],IMG_SIZE[1],8)
    def get_model(self,num_layers_to_fine_tune=0):
        return AllVsAllTrainer.get_model( self,num_layers_to_fine_tune=num_layers_to_fine_tune)

class SimpleModelAVATrainer(Trainer):
    def __init__(self,
            epochs=EPOCHS,batch_size=BATCH_SIZE,
            model_path="models",
            dataset_dir="dataset",
            dataset="ck",
            model_name_suffix = None,
            verbose = True,
            learnining_rate = 1e-4,
            model_name = "simple",
            augmentation = False,
            steps_per_epoch=1000
            ):
        Trainer.__init__(self,epochs,batch_size,model_path,dataset_dir,dataset,model_name_suffix,verbose,learnining_rate)
        print self.dataset_dir
        print self.dataset

        self.model_name = model_name
        self.augmentation = augmentation
        self.steps_per_epoch = steps_per_epoch
    def get_model(self):
        print "gettting model"
        model = Sequential()
        model.add(Conv2D(32, kernel_size=(7, 7), activation='relu', input_shape=(IMG_SIZE[0], IMG_SIZE[1], 1)))
        model.add(Conv2D(64, (7, 7), activation='relu'))
        model.add(Dropout(0.1))
        model.add(Conv2D(64, (5, 5), activation='relu'))
        model.add(Dropout(0.1))
        model.add(Conv2D(64, (5, 5), activation='relu'))
        model.add(Dropout(0.1))
        model.add(Conv2D(64, (5, 5), activation='relu'))
        model.add(Dropout(0.1))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(128, (3, 3), activation='relu'))
        model.add(Dropout(0.1))
        model.add(Conv2D(128, (3, 3), activation='relu'))
        model.add(Dropout(0.1))
        model.add(Conv2D(128, (3, 3), activation='relu'))
        model.add(Dropout(0.1))
        model.add(Conv2D(128, (3, 3), activation='relu'))
        model.add(Dropout(0.1))
        model.add(Conv2D(128, (3, 3), activation='relu'))
        model.add(Dropout(0.1))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        model.add(Dense(252, activation='relu'))
        model.add(Dropout(0.2))
        model.add(Dense(1024, activation='relu'))
        model.add(Dropout(0.2))
        model.add(Dense(7, activation='softmax'))

        model.compile(loss=keras.losses.categorical_crossentropy,
                    optimizer=keras.optimizers.Adam(self.learnining_rate),
                    metrics=['accuracy'])
        return model
    def save_and_log_model(self,model,x_test,y_test):
        model_json = model.to_json()
        with open( os.path.join(self.model_path ,self.dataset+"-" + self.model_name + ".json"), "w+") as json_file:
            json_file.write(model_json)
        model.save_weights(os.path.join(self.model_path ,self.dataset +"-" + self.model_name + ".h5"))
        score = model.evaluate(x_test, y_test, batch_size=self.batch_size)
        with open(os.path.join("logs",self.dataset +"-" + self.model_name+".txt"),"a+") as logfile:
            logfile.write(self.model_name+":")
            logfile.write(str(score))
            logfile.write("\n")
        if self.verbose:
            print("Saved model to disk")

    def train(self):
        
        x_train,x_test,y_train_all,y_test_all = self.get_datasets()
        with open(os.path.join("logs",self.dataset +"-" + self.model_name+".txt"),"a+") as logfile:
            if self.verbose:
                print "logs are written to ",os.path.join("logs",self.dataset +"-" + self.model_name+".txt")
            a = datetime.datetime.now()
            a = a.strftime("%a %b %d %Y %H:%M:%S")
            logfile.write("_"*(2*len(a))+"\n")
            logfile.write(a)
            logfile.write("\n")
            logfile.write("Training with simple all vs all model\n")

            logfile.write("Training on dataset: "+self.dataset + ", With the following parameters")
            logfile.write("\n")
            logfile.write("-"*len(a))
            logfile.write("\n")
            logfile.write("Epochs "+str(self.epochs))
            logfile.write("\n")
            logfile.write("Batch-size "+str(self.batch_size))
            logfile.write("\n")
            logfile.write("Augmentation "+str(self.augmentation))
            logfile.write("\n")
            try:
                logfile.write("Learning-rate "+self.learnining_rate)
            except:
                pass
            
            logfile.write("-"*len(a))
            logfile.write("\n")
        y_train = np.eye(7)[y_train_all]
        y_test = np.eye(7)[y_test_all]
        model = self.get_model()
        if (self.verbose):
            print "Modeling all vs all model on dataset ",self.dataset
        if self.augmentation:
            datagen=ImageDataGenerator(
                rotation_range=40,
                horizontal_flip=True,
                shear_range=0.2,
                width_shift_range=0.2,
                height_shift_range=0.2,
                zoom_range=0.2,
                # equalize_adapthist=True
            )
            datagen.fit(x_train)
            model.fit_generator(datagen.flow(x_train,y_train,
                batch_size=self.batch_size
            ),steps_per_epoch=self.steps_per_epoch,
                        epochs=self.epochs,
                        validation_data=(x_test, y_test))
        else:
            model.fit(x_train, y_train,
                    batch_size=self.batch_size,
                    epochs=self.epochs,
                    verbose=1,
                    validation_data=(x_test, y_test))
        self.save_and_log_model(model,x_test,y_test)
class MultipleRepresentationTrainer(Trainer):
    def __init__(self,
            epochs=EPOCHS,batch_size=BATCH_SIZE,
            model_path="models",
            dataset_dir="dataset",
            dataset="ck",
            model_name_suffix = None,
            verbose = True,
            learnining_rate = 1e-4,
            model_name = "multirep",
            augmentation = False,
            steps_per_epoch = 1000
            ):
        self.predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
        Trainer.__init__(self,epochs,batch_size,model_path,dataset_dir,dataset,model_name_suffix,verbose,learnining_rate)
        self.model_name = model_name
        self.augmentation = augmentation
        self.steps_per_epoch = steps_per_epoch
    
    def get_datasets(self):
        x_train_all , y_train_all = load_dataset(os.path.join(self.dataset_dir,self.dataset,"train"))
        x_test_all , y_test_all = load_dataset(os.path.join(self.dataset_dir,self.dataset,"test"))

        x_train_all, y_train_all = shuffle(x_train_all,y_train_all)
        x_test_all , y_test_all  = shuffle(x_test_all, y_test_all)
        
        
        x_train,x_test,y_train_all,y_test_all = x_train_all,x_test_all, y_train_all,y_test_all
        dlibpoints_train,centroid_train = to_dlib_points(x_train,self.predictor)
        dlibpoints_test,centroid_test = to_dlib_points(x_test,self.predictor)
        dist_train,angle_train = get_distances_angles(dlibpoints_train,centroid_train)
        dist_test,angle_test = get_distances_angles(dlibpoints_test,centroid_test)

        # normalize
        dlibpoints_train /= 50.0;
        dlibpoints_train = dlibpoints_train.reshape((-1,1,68,2))
        dlibpoints_test /= 50.0;
        dlibpoints_test = dlibpoints_test.reshape((-1,1,68,2))

        dist_train /= 50.0;
        dist_train = dist_train.reshape(-1,1,68,1)
        dist_test /= 50.0;
        dist_test = dist_test.reshape(-1,1,68,1)

        angle_train /= (2 * np.pi)
        angle_train = angle_train.reshape(-1,1,68,1)
        angle_test /= (2 * np.pi)
        angle_test = angle_test.reshape(-1,1,68,1)

        x_train = x_train.reshape((-1,IMG_SIZE[0],IMG_SIZE[1],1)).astype("float32")/255
        x_test  = x_test.reshape((-1,IMG_SIZE[0],IMG_SIZE[1],1)).astype("float32")/255
        
        return x_train,x_test, dlibpoints_train,dlibpoints_test,dist_train,dist_test,angle_train,angle_test,y_train_all,y_test_all
    def get_model(self):
        image_input = Input(shape=(IMG_SIZE[0],IMG_SIZE[1],1))
        image_layer = Conv2D(32,kernel_size=(7, 7),activation="relu", padding="SAME")(image_input)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(32,  (7, 7), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(32,  (7, 7), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(32,  (7, 7), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(32,  (7, 7), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = MaxPooling2D(pool_size=(2, 2), padding="SAME")(image_layer)
        image_layer = Conv2D(64, (5, 5), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(64, (5, 5), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(64, (5, 5), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(64, (5, 5), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = MaxPooling2D(pool_size=(2, 2), padding="SAME")(image_layer)
        image_layer = Conv2D(128, (3, 3), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(128, (3, 3), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(128, (3, 3), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(128, (3, 3), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = Conv2D(128, (3, 3), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = MaxPooling2D(pool_size=(2, 2), padding="SAME")(image_layer)
        image_layer = Conv2D(1024, (3, 3), activation='relu', padding="SAME")(image_layer)
        image_layer = Dropout(0.2)(image_layer)
        image_layer = MaxPooling2D(pool_size=(2, 2), padding="SAME")(image_layer)
        image_layer = Flatten()(image_layer)
        

        dlib_points_input = Input(shape=(1,68,2))
        dlib_points_layer = Conv2D(32,kernel_size=(1,3),activation="relu", padding="SAME")(dlib_points_input)
        dlib_points_layer = Conv2D(32, (1,3),activation="relu", padding="SAME")(dlib_points_layer)
        dlib_points_layer = MaxPooling2D(pool_size=(1,2), padding="SAME")(dlib_points_layer)
        dlib_points_layer = Conv2D(64,(1,3),activation="relu", padding="SAME")(dlib_points_layer)
        dlib_points_layer = MaxPooling2D(pool_size=(1,2), padding="SAME")(dlib_points_layer)
        dlib_points_layer = Flatten()(dlib_points_layer)


        distance_input = Input(shape=(1,68,1))
        distance_layer = Conv2D(32, padding="SAME",kernel_size=(1,3),activation="relu")(distance_input)
        distance_layer = Conv2D(32, (1,3),padding="SAME",activation="relu")(distance_layer)
        distance_layer = MaxPooling2D(pool_size=(1,2), padding="SAME")(distance_layer)
        distance_layer = Conv2D(64, (1,3), padding="SAME",activation="relu")(distance_layer)
        distance_layer = MaxPooling2D(pool_size=(1,2), padding="SAME")(distance_layer)
        
        distance_layer = Flatten()(distance_layer)

        angle_layer_input = Input(shape=(1,68,1))
        angle_layer = Conv2D(32,kernel_size=(1,3),activation="relu", padding="SAME")(angle_layer_input)
        angle_layer = Conv2D(32, (1,3),activation="relu", padding="SAME")(angle_layer)
        angle_layer = MaxPooling2D(pool_size=(1,2), padding="SAME")(angle_layer)
        angle_layer = Conv2D(64, (1,3),activation="relu", padding="SAME")(angle_layer)
        angle_layer = MaxPooling2D(pool_size=(1,2), padding="SAME")(angle_layer)
        angle_layer = Flatten()(angle_layer)

        merged_layers = keras.layers.concatenate([image_layer,dlib_points_layer,distance_layer,angle_layer])
        output = Dense(128,activation="relu")(merged_layers)
        output = Dropout(0.2)(output)
        output = Dense(252,activation="relu")(output)
        output = Dropout(0.2)(output)
        output = Dense(2,activation="softmax")(output)
        model = Model(inputs=[image_input,dlib_points_input,distance_input,angle_layer_input],outputs=output)
        model.compile(loss=keras.losses.categorical_crossentropy,
                    optimizer=keras.optimizers.Adam(self.learnining_rate),
                    metrics=['accuracy'])
        return model
    def train(self):
        

        

        

        with open(os.path.join("logs",self.dataset +"-" + self.model_name+".txt"),"a+") as logfile:
            if self.verbose:
                print "logs are written to ",os.path.join("logs",self.dataset +"-" + self.model_name+".txt")
            a = datetime.datetime.now()
            a = a.strftime("%a %b %d %Y %H:%M:%S")
            logfile.write("_"*(2*len(a))+"\n")
            logfile.write(a)
            logfile.write("\n")
            logfile.write("Training with simple all vs all model\n")

            logfile.write("Training on dataset: "+self.dataset + ", With the following parameters")
            logfile.write("\n")
            logfile.write("-"*len(a))
            logfile.write("\n")
            logfile.write("Epochs "+str(self.epochs))
            logfile.write("\n")
            logfile.write("Batch-size "+str(self.batch_size))
            logfile.write("\n")
            try:
                logfile.write("Learning-rate "+self.learnining_rate)
            except:
                pass
            
            logfile.write("-"*len(a))
            logfile.write("\n")

        model = self.get_model()
        model.summary()
        x_train_all , y_train_all = load_dataset(os.path.join(self.dataset_dir,self.dataset,"train"))
        x_test_all , y_test_all = load_dataset(os.path.join(self.dataset_dir,self.dataset,"test"))

        x_train_all, y_train_all = shuffle(x_train_all,y_train_all)
        x_test_all , y_test_all  = shuffle(x_test_all, y_test_all)
        
        
        x_train,x_test,y_train_all,y_test_all = x_train_all,x_test_all, y_train_all,y_test_all

        y_train = np.eye(2)[y_train_all]
        y_test = np.eye(2)[y_test_all]

        # normalize
        
        dlibpoints_test,centroid_test = to_dlib_points(x_test,self.predictor)
        dist_test,angle_test = get_distances_angles(dlibpoints_test,centroid_test)
        dlibpoints_test /= 50.0;
        dlibpoints_test = dlibpoints_test.reshape((-1,1,68,2))
        dist_test /= 50.0;
        dist_test = dist_test.reshape(-1,1,68,1)
        angle_test /= (2 * np.pi)
        angle_test = angle_test.reshape(-1,1,68,1)
        x_test = x_test.astype(float)/255.0
        if (self.verbose):
            print "Modeling multi representation and all vs all model on dataset ",self.dataset
        x_test = x_test.reshape(-1,48,48,1)
        test_inputs = [x_test,dlibpoints_test,dist_test,angle_test]
        checkpoint = callbacks.ModelCheckpoint('models/'+self.dataset+"-"+"-"+self.model_name+'-{epoch:02d}-acc-{val_acc:0.3f}.h5',
                    save_best_only=True, save_weights_only=True, verbose=1)
        if not self.augmentation:
            dlibpoints_train,centroid_train = to_dlib_points(x_train,self.predictor)
            dist_train,angle_train = get_distances_angles(dlibpoints_train,centroid_train)
            dlibpoints_train /= 50.0;
            dlibpoints_train = dlibpoints_train.reshape((-1,1,68,2))
            dist_train /= 50.0;
            dist_train = dist_train.reshape(-1,1,68,1)
            angle_train /= (2 * np.pi)
            angle_train = angle_train.reshape(-1,1,68,1)
            x_train = x_train.astype(float)/255
            x_train = x_train.reshape(x_train.shape[0],x_train.shape[1],x_train.shape[2],1)
            train_inputs = [x_train,dlibpoints_train,dist_train,angle_train]
            model.fit(train_inputs, y_train,
                    batch_size=self.batch_size,
                    epochs=self.epochs,
                    verbose=1,
                    callbacks = [checkpoint],
                    validation_data=(test_inputs, y_test))
        else:
            datagen=EmopyMultiRepDataGenerator(
                rotation_range=20,
                horizontal_flip=True,
                shear_range=0.2,
                width_shift_range=0.2,
                height_shift_range=0.2,
                zoom_range=0.2,
                predictor= self.predictor
                # equalize_adapthist=True
            )
            x_train = x_train.reshape(x_train.shape[0],x_train.shape[1],x_train.shape[2],1)
            datagen.fit(x_train)

            
            model.fit_generator(datagen.generate(x_train,y_train,
            ),steps_per_epoch=self.steps_per_epoch,
                        epochs=self.epochs,
                        callbacks = [checkpoint],
                        validation_data=(test_inputs, y_test))
        self.save_and_log_model(model,test_inputs,y_test)

    def save_and_log_model(self,model,x_test,y_test):
        model_json = model.to_json()
        with open( os.path.join(self.model_path ,self.dataset+"-" + self.model_name + ".json"), "w+") as json_file:
            json_file.write(model_json)
        model.save_weights(os.path.join(self.model_path ,self.dataset +"-" + self.model_name + ".h5"))
        score = model.evaluate(x_test, y_test, batch_size=self.batch_size)
        with open(os.path.join("logs",self.dataset +"-" + self.model_name+".txt"),"a+") as logfile:
            logfile.write(self.model_name+":")
            logfile.write(str(score))
            logfile.write("\n")
        if self.verbose:
            print("Saved model to disk")
    